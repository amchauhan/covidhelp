﻿using Microsoft.AspNetCore.Mvc;
using Nop.Services.Medical;
using Nop.Web.Models.Equipments;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Nop.Web.Controllers
{
    public class AvailableEquipmentController : BasePublicController
    {
        private readonly IEquipmentsService _equipmentService;

        public AvailableEquipmentController(IEquipmentsService equipmentService) 
        {
            _equipmentService = equipmentService;
        }

        [HttpGet]
        public JsonResult GetFilterData(int areaId)
        {
            List<CenterContentModel> model = new List<CenterContentModel>();
            var centerList = _equipmentService.GetAllCovidCenters(areaId: areaId);
            foreach (var item in centerList)
            {
                var centerEquipments = _equipmentService.GetEquipmentsByCustomerId(item.CustomerId).ToList();

                var center = new CenterContentModel
                {
                    Name = item.Name,
                    Address = item.Address,
                    Contact = item.ContactNo,
                    City = _equipmentService.GetCityById(item.City).Name,
                    Area = _equipmentService.GetAreaById(item.Area).Name,
                    Bed = centerEquipments.Where(x => x.CategoryId == 1).FirstOrDefault().Quantity,
                    Vantilator = centerEquipments.Where(x => x.CategoryId == 2).FirstOrDefault().Quantity,
                    Oxygen = centerEquipments.Where(x => x.CategoryId == 3).FirstOrDefault().Quantity,
                };

                model.Add(center);
            }

            var HTMLString = RenderPartialViewToString("_ContentData", model);

            return Json(HTMLString);
        }
    }
}
