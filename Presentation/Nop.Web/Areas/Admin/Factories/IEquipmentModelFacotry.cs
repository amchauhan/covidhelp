﻿using Nop.Core.Domain.Customers;
using Nop.Core.Domain.Medical;
using Nop.Web.Areas.Admin.Models.Equipments;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Nop.Web.Areas.Admin.Factories
{
    public partial interface IEquipmentModelFacotry
    {
        EquipmentsListModel PrepareEquipmentsListModel(EquipmentsSearchModel searchModel,
            Customer customer);

        EquipmentsModel PrepareEquipmentsModel(EquipmentsModel model, Customer customer, Equipments equipment);
    }
}
