﻿using Microsoft.AspNetCore.Mvc.Rendering;
using Nop.Core.Domain.Customers;
using Nop.Core.Domain.Medical;
using Nop.Services.Catalog;
using Nop.Services.Customers;
using Nop.Services.Medical;
using Nop.Web.Areas.Admin.Infrastructure.Mapper.Extensions;
using Nop.Web.Areas.Admin.Models.Equipments;
using Nop.Web.Framework.Models.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Nop.Web.Areas.Admin.Factories
{
    public partial class EquipmentModelFacotry : IEquipmentModelFacotry
    {
        #region FIelds
        private readonly IEquipmentsService _equipmentsService;
        private readonly ICategoryService _categoryService;
        #endregion


        #region Ctor
        public EquipmentModelFacotry(IEquipmentsService equipmentsService,
            ICategoryService categoryService) 
        {
            _equipmentsService = equipmentsService;
            _categoryService = categoryService;
        }
        #endregion

        #region Methods

        public EquipmentsListModel PrepareEquipmentsListModel(EquipmentsSearchModel searchModel, Customer customer)
        {
            if (searchModel == null)
                throw new ArgumentNullException(nameof(searchModel));

            if (customer == null)
                throw new ArgumentNullException(nameof(customer));

            
            var equipments = _equipmentsService
                .GetEquipmentsByCustomerId(customer.Id).ToPagedList(searchModel);

            //prepare grid model
            var model = new EquipmentsListModel().PrepareToGrid(searchModel, equipments, () =>
            {
                return equipments.Select(eq =>
                {
                    //fill in model values from the entity
                    var equipmentsModel = eq.ToModel<EquipmentsModel>();

                    //fill in additional values (not existing in the entity)
                    equipmentsModel.EquipmentName = _categoryService.GetCategoryById(eq.CategoryId)?.Name;
                    
                    return equipmentsModel;
                });
            });

            return model;

        }

        public EquipmentsModel PrepareEquipmentsModel(EquipmentsModel model, Customer customer, Equipments equipment)
        {
            if (customer == null)
                throw new ArgumentNullException(nameof(customer));

            if (equipment != null)
            {
                model = equipment.ToModel<EquipmentsModel>();
            }
            model.CustomerId = customer.Id;
            model.AvailableEquipments = _categoryService.GetAllCategories().Select(x => new SelectListItem { 
                Text = x.Name,
                Value = x.Id.ToString()
            }).ToList();

            model.AvailableEquipments.Insert(0, new SelectListItem { Text = "Select", Value = "0" });

            return model;
        }
        #endregion
    }
}
