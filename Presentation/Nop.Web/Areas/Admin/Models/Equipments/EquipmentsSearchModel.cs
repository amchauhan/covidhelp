﻿using Nop.Web.Framework.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Nop.Web.Areas.Admin.Models.Equipments
{
    public partial class EquipmentsSearchModel : BaseSearchModel
    {
        public int CustomerId { get; set; }

        public string CustomerName { get; set; }
    }
}
