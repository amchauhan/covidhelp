﻿using Microsoft.AspNetCore.Mvc.Rendering;
using Nop.Web.Framework.Models;
using Nop.Web.Framework.Mvc.ModelBinding;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Nop.Web.Areas.Admin.Models.Equipments
{
    public partial class EquipmentsModel : BaseNopEntityModel
    {
        public EquipmentsModel() 
        {
            AvailableEquipments = new List<SelectListItem>();
        }


        public int CustomerId { get; set; }

        [NopResourceDisplayName("Admin.Equipments.Fields.EquipmentsName")]
        public int CategoryId { get; set; }

        [NopResourceDisplayName("Admin.Equipments.Fields.Quantity")]
        public int Quantity { get; set; }
        
        [NopResourceDisplayName("Admin.Equipments.Fields.CustomerName")]
        public string CustomerName { get; set; }
        
        [NopResourceDisplayName("Admin.Equipments.Fields.EquipmentsName")]
        public string EquipmentName { get; set; }

        public IList<SelectListItem> AvailableEquipments { get; set; }

    }
}
