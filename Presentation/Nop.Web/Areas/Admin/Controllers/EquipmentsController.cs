﻿using Microsoft.AspNetCore.Mvc;
using Nop.Core;
using Nop.Core.Domain.Medical;
using Nop.Services.Customers;
using Nop.Services.Localization;
using Nop.Services.Medical;
using Nop.Services.Messages;
using Nop.Web.Areas.Admin.Factories;
using Nop.Web.Areas.Admin.Infrastructure.Mapper.Extensions;
using Nop.Web.Areas.Admin.Models.Equipments;
using Nop.Web.Framework.Mvc.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Nop.Web.Areas.Admin.Controllers
{
    public class EquipmentsController : BaseAdminController
    {
        #region Fields
        private readonly IEquipmentModelFacotry _equipmentModelFacotry;
        private readonly IEquipmentsService _equipmentsService;
        private readonly IWorkContext _workContext;
        private readonly INotificationService _notificationService;
        private readonly ILocalizationService _localizationService;
        private readonly ICustomerService _customerService;
        #endregion

        #region Ctor
        public EquipmentsController(IEquipmentModelFacotry equipmentModelFacotry,
            IEquipmentsService equipmentsService,
            IWorkContext workContext,
            INotificationService notificationService,
            ILocalizationService localizationService,
            ICustomerService customerService) 
        {
            _equipmentModelFacotry = equipmentModelFacotry;
            _equipmentsService = equipmentsService;
            _workContext = workContext;
            _notificationService = notificationService;
            _localizationService = localizationService;
            _customerService = customerService;
        }
        #endregion

        #region Methods
        public IActionResult List()
        {
            var model = new EquipmentsSearchModel();
            model.CustomerId = _workContext.CurrentCustomer.Id;

            model.CustomerName = _customerService.GetCustomerFullName(_workContext.CurrentCustomer);


            return View(model);
        }

        [HttpPost]
        public IActionResult List(EquipmentsSearchModel searchModel) 
        {
            var model = _equipmentModelFacotry.PrepareEquipmentsListModel(searchModel, _workContext.CurrentCustomer);

            return Json(model);
        }

        public IActionResult Create() 
        {
            var model = _equipmentModelFacotry.PrepareEquipmentsModel(new EquipmentsModel(), _workContext.CurrentCustomer, null);

            return View(model);
        }

        [HttpPost, ParameterBasedOnFormName("save-continue", "continueEditing")]
        public IActionResult Create(EquipmentsModel model, bool continueEditing)
        {
            if (ModelState.IsValid)
            {
                if (_equipmentsService.GetEquipmentsByCustomerId(model.CustomerId).Any(x => x.CategoryId == model.CategoryId))
                {
                    //redisplay form
                    _notificationService.ErrorNotification(_localizationService.GetResource("Admin.Equipment.AlreadyExists"));

                    model = _equipmentModelFacotry.PrepareEquipmentsModel(model, _workContext.CurrentCustomer, null);

                    return View(model);
                }

                //insert mapping
                var newEquipmentMapping = model.ToEntity<Equipments>();
                _equipmentsService.InsertEquipments(newEquipmentMapping);

                _notificationService.SuccessNotification(_localizationService.GetResource("Admin.Equipment.Added"));

                if (!continueEditing)
                {
                    return RedirectToAction("List");
                }

                return RedirectToAction("Edit", new { id = newEquipmentMapping.Id });
            }

            return RedirectToAction("List");
        }

        public IActionResult Edit(int id)
        {
            var equipmentMapping = _equipmentsService.GetEquipmentsById(id);

            //prepare model
            var model = _equipmentModelFacotry.PrepareEquipmentsModel(new EquipmentsModel(), _workContext.CurrentCustomer, equipmentMapping);

            return View(model);
        }

        [HttpPost, ParameterBasedOnFormName("save-continue", "continueEditing")]
        public IActionResult Edit(EquipmentsModel model, bool continueEditing)
        {
            if (ModelState.IsValid)
            {
                var equipmentMapping = _equipmentsService.GetEquipmentsById(model.Id);

                //update mapping
                equipmentMapping = model.ToEntity(equipmentMapping);
                _equipmentsService.UpdateEquipments(equipmentMapping);

                _notificationService.SuccessNotification(_localizationService.GetResource("Admin.Equipment.Updated"));

                if (!continueEditing)
                {
                    return RedirectToAction("Edit", new { id = equipmentMapping.Id });
                }
            }

            return RedirectToAction("List");
        }

        [HttpPost]
        public IActionResult Delete(int id) 
        {
            var equipmentMapping = _equipmentsService.GetEquipmentsById(id);
            if (equipmentMapping != null)
            {
                _equipmentsService.DeleteEquipments(equipmentMapping);
                _notificationService.SuccessNotification(_localizationService.GetResource("Admin.Equipment.Deleted"));
            }
            return RedirectToAction("List");
        }
        #endregion
    }
}
