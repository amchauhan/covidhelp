﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Nop.Services.Medical;
using Nop.Web.Framework.Components;
using Nop.Web.Models.Equipments;
using System.Collections.Generic;
using System.Linq;

namespace Nop.Web.Components
{
    public class AvailableEquipmentsViewComponent : NopViewComponent
    {
        private readonly IEquipmentsService _equipmentService;
        public AvailableEquipmentsViewComponent(IEquipmentsService equipmentService) 
        {
            _equipmentService = equipmentService;
        }

        public IViewComponentResult Invoke()
        {
            AvailableCenterModel model = new AvailableCenterModel();
            var centerList = _equipmentService.GetAllCovidCenters();
            foreach (var item in centerList)
            {
                var centerEquipments = _equipmentService.GetEquipmentsByCustomerId(item.CustomerId).ToList();

                var center = new CenterContentModel
                {
                    Name = item.Name,
                    Address = item.Address,
                    Contact = item.ContactNo,
                    City = _equipmentService.GetCityById(item.City).Name,
                    Area = _equipmentService.GetAreaById(item.Area).Name,
                    Bed = centerEquipments.Where(x => x.CategoryId == 1).FirstOrDefault().Quantity,
                    Vantilator = centerEquipments.Where(x => x.CategoryId == 2).FirstOrDefault().Quantity,
                    Oxygen = centerEquipments.Where(x => x.CategoryId == 3).FirstOrDefault().Quantity,
                };

                model.CenterData.Add(center);
            }

            model.AvailableAreas = _equipmentService.GetAllAreasByCity(1).Select(x => new SelectListItem { Text = x.Name, Value = x.Id.ToString() }).ToList();
            model.AvailableAreas.Insert(0, new SelectListItem { Text = "Select", Value = "0" });
            model.SelectedAreaId = 0;

            return View(model);
        }
    }
}
