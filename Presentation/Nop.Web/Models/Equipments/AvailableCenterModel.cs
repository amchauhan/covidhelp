﻿using Microsoft.AspNetCore.Mvc.Rendering;
using Nop.Web.Framework.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Nop.Web.Models.Equipments
{
    public class AvailableCenterModel : BaseNopModel
    {
        public AvailableCenterModel() 
        {
            CenterData = new List<CenterContentModel>();
            AvailableAreas = new List<SelectListItem>();
        }

        public IList<CenterContentModel> CenterData { get; set; }
        public int SelectedAreaId { get; set; }
        public IList<SelectListItem> AvailableAreas { get; set; }
    }

    public class CenterContentModel 
    {
        public string Name { get; set; }
        public string Address { get; set; }
        public string Contact { get; set; }
        public string City { get; set; }
        public string Area { get; set; }
        public int Bed { get; set; }
        public int Vantilator { get; set; }
        public int Oxygen { get; set; }
    }
}
