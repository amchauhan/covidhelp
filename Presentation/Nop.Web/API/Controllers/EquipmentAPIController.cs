﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Nop.Services.Medical;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Nop.Web.API.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class EquipmentAPIController : ControllerBase
    {
        private readonly IEquipmentsService _equipmentsService;

        public EquipmentAPIController(IEquipmentsService equipmentsService) 
        {
            this._equipmentsService = equipmentsService;
        }

        [HttpPost(Name = nameof(GetEquipments))]
        public IActionResult GetEquipments(int areaId) 
        {
            var equipments = _equipmentsService.GetAllCovidCenters(areaId: areaId);

            return Ok(equipments);
        }
    }
}
