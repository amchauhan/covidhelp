﻿using Nop.Core;
using Nop.Core.Domain.Medical;
using System;
using System.Collections.Generic;
using System.Text;

namespace Nop.Services.Medical
{
    public interface IEquipmentsService
    {
        #region Centers

        void DeleteCovidCenter(CovidCenters covidCenters);

        IList<CovidCenters> GetAllCovidCenters(string name = null, int cityId = 1, int areaId = 0);

        void InsertCovidCenter(CovidCenters covidCenters);

        void UpdateCovidCenter(CovidCenters covidCenters);
        #endregion

        #region Equipments

        void DeleteEquipments(Equipments equipments);

        IPagedList<Equipments> GetAllEquipments(int pageIndex = 0, int pageSize = int.MaxValue);

        Equipments GetEquipmentsById(int equipmentsId);
        
        IList<Equipments> GetEquipmentsByCustomerId(int customerId);

        void InsertEquipments(Equipments equipments);

        void UpdateEquipments(Equipments equipments);
        #endregion

        #region City

        IList<CityNames> GetAllCities();

        CityNames GetCityById(int cityId);
        #endregion

        #region Areas

        IList<AreasName> GetAllAreas();

        IList<AreasName> GetAllAreasByCity(int cityId);

        AreasName GetAreaById(int areaId);
        #endregion


    }
}
