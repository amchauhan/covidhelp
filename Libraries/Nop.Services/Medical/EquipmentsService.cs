﻿using Nop.Core;
using Nop.Core.Domain.Medical;
using Nop.Data;
using Nop.Services.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Nop.Services.Medical
{
    public partial class EquipmentsService : IEquipmentsService
    {
        #region Fields
        private readonly IRepository<Equipments> _equipmentsRepository;
        private readonly IRepository<CityNames> _cityNamesRepository;
        private readonly IRepository<AreasName> _areaNamesRepository;
        private readonly IRepository<CovidCenters> _covidCentersRepository;
        private readonly IEventPublisher _eventPublisher;
        #endregion


        #region Ctor
        public EquipmentsService(IRepository<Equipments> equipmentsRepository,
            IRepository<CityNames> cityNamesRepository,
            IRepository<AreasName> areaNamesRepository,
            IRepository<CovidCenters> covidCentersRepository,
            IEventPublisher eventPublisher) 
        {
            _equipmentsRepository = equipmentsRepository;
            _cityNamesRepository = cityNamesRepository;
            _areaNamesRepository = areaNamesRepository;
            _covidCentersRepository = covidCentersRepository;
            _eventPublisher = eventPublisher;
        }
        #endregion

        #region Methods

        #region Centers

        public void DeleteCovidCenter(CovidCenters covidCenters) 
        {
            if (covidCenters == null)
                throw new ArgumentNullException(nameof(covidCenters));

            _covidCentersRepository.Delete(covidCenters);

            //event notification
            _eventPublisher.EntityDeleted(covidCenters);
        }

        public IList<CovidCenters> GetAllCovidCenters(string name = null, int cityId = 1, int areaId = 0) 
        {
            var query = _covidCentersRepository.Table;

            query = query.Where(x => x.City == cityId);

            if (!string.IsNullOrEmpty(name))
            {
                query = query.Where(x => x.Name.ToLower() == name.ToLower());
            }

            if (areaId > 0)
            {
                query = query.Where(x => x.Area == areaId);
            }

            return query.ToList();
        }

        public void InsertCovidCenter(CovidCenters covidCenters) 
        {
            if (covidCenters == null)
                throw new ArgumentNullException(nameof(covidCenters));

            _covidCentersRepository.Insert(covidCenters);

            //event notification
            _eventPublisher.EntityDeleted(covidCenters);
        }

        public void UpdateCovidCenter(CovidCenters covidCenters) 
        {
            if (covidCenters == null)
                throw new ArgumentNullException(nameof(covidCenters));

            _covidCentersRepository.Update(covidCenters);

            //event notification
            _eventPublisher.EntityDeleted(covidCenters);
        }
        #endregion

        #region Equipments
        public void DeleteEquipments(Equipments equipments)
        {
            if (equipments == null)
                throw new ArgumentNullException(nameof(equipments));

            _equipmentsRepository.Delete(equipments);

            //event notification
            _eventPublisher.EntityDeleted(equipments);
        }

        public IPagedList<Equipments> GetAllEquipments(int pageIndex = 0, int pageSize = int.MaxValue)
        {
            var query = from eq in _equipmentsRepository.Table
                        select eq;

            var equipments = new PagedList<Equipments>(query, pageIndex, pageSize);

            return equipments;
        }

        public IList<Equipments> GetEquipmentsByCustomerId(int customerId)
        {
            if (customerId == 0)
                return null;

            var query = from eq in _equipmentsRepository.Table
                        where eq.CustomerId == customerId
                        select eq;

            return query.ToList();
        }

        public Equipments GetEquipmentsById(int equipmentsId)
        {
            if (equipmentsId == 0)
                return null;

            return _equipmentsRepository.GetById(equipmentsId);
        }

        public void InsertEquipments(Equipments equipments)
        {
            if (equipments == null)
                throw new ArgumentNullException(nameof(equipments));

            _equipmentsRepository.Insert(equipments);

            //event notification
            _eventPublisher.EntityInserted(equipments);
        }

        public void UpdateEquipments(Equipments equipments)
        {
            if (equipments == null)
                throw new ArgumentNullException(nameof(equipments));

            _equipmentsRepository.Update(equipments);

            //event notification
            _eventPublisher.EntityUpdated(equipments);
        }
        #endregion

        #region City

        public IList<CityNames> GetAllCities() 
        {
            var query = from ct in _cityNamesRepository.Table
                        select ct;

            return query.ToList();
        }

        public CityNames GetCityById(int cityId) 
        {
            if (cityId == 0)
                return null;

            return _cityNamesRepository.GetById(cityId);
        }
        #endregion

        #region Areas

        public IList<AreasName> GetAllAreas() 
        {
            var query = from ar in _areaNamesRepository.Table
                        select ar;

            return query.ToList();
        }

        public IList<AreasName> GetAllAreasByCity(int cityId) 
        {
            var query = from ar in _areaNamesRepository.Table
                        where ar.CityId == cityId
                        select ar;

            return query.ToList();
        }

        public AreasName GetAreaById(int areaId) 
        {
            if (areaId == 0)
                return null;

            return _areaNamesRepository.GetById(areaId);
        }
        #endregion

        #endregion
    }
}
