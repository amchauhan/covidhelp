﻿using FluentMigrator.Builders.Create.Table;
using Nop.Core.Domain.Medical;
using System;
using System.Collections.Generic;
using System.Text;

namespace Nop.Data.Mapping.Builders.Medical
{
    public partial class EquipmentsBuilder : NopEntityBuilder<Equipments>
    {
        #region Methods

        public override void MapEntity(CreateTableExpressionBuilder table)
        {
            table
                .WithColumn(nameof(Equipments.CustomerId)).AsInt32().NotNullable()
                .WithColumn(nameof(Equipments.CategoryId)).AsInt32().NotNullable()
                .WithColumn(nameof(Equipments.Quantity)).AsInt32().NotNullable();
        }

        #endregion
    }
}
