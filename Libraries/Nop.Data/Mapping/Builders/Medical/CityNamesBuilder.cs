﻿using FluentMigrator.Builders.Create.Table;
using Nop.Core.Domain.Medical;
using System;
using System.Collections.Generic;
using System.Text;

namespace Nop.Data.Mapping.Builders.Medical
{
    public partial class CityNamesBuilder : NopEntityBuilder<CityNames>
    {
        public override void MapEntity(CreateTableExpressionBuilder table)
        {
            table
                .WithColumn(nameof(CityNames.Name)).AsString(400).NotNullable();
        }
    }
}
