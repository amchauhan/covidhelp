﻿using FluentMigrator.Builders.Create.Table;
using Nop.Core.Domain.Medical;
using System;
using System.Collections.Generic;
using System.Text;

namespace Nop.Data.Mapping.Builders.Medical
{
    public partial class AreaNamesBuilder : NopEntityBuilder<AreasName>
    {
        public override void MapEntity(CreateTableExpressionBuilder table)
        {
            table
                .WithColumn(nameof(AreasName.CityId)).AsInt32().NotNullable()
                .WithColumn(nameof(AreasName.Name)).AsString(400).NotNullable();
        }
    }
}
