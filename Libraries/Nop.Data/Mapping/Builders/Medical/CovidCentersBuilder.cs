﻿using FluentMigrator.Builders.Create.Table;
using Nop.Core.Domain.Medical;
using System;
using System.Collections.Generic;
using System.Text;

namespace Nop.Data.Mapping.Builders.Medical
{
    public partial class CovidCentersBuilder : NopEntityBuilder<CovidCenters>
    {
        public override void MapEntity(CreateTableExpressionBuilder table)
        {
            table
                .WithColumn(nameof(CovidCenters.CustomerId)).AsInt32().NotNullable()
                .WithColumn(nameof(CovidCenters.Name)).AsString(400).NotNullable()
                .WithColumn(nameof(CovidCenters.Address)).AsString(400).NotNullable()
                .WithColumn(nameof(CovidCenters.City)).AsInt32().NotNullable()
                .WithColumn(nameof(CovidCenters.Area)).AsInt32().NotNullable()
                .WithColumn(nameof(CovidCenters.ContactNo)).AsString(400).NotNullable()
                .WithColumn(nameof(CovidCenters.Email)).AsString(400).NotNullable();

        }
    }
}
