﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Nop.Core.Domain.Medical
{
    public partial class CovidCenters  :BaseEntity
    {
        public int CustomerId { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
        public string Email { get; set; }
        public string ContactNo { get; set; }
        public int City { get; set; }
        public int Area { get; set; }
    }
}
