﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Nop.Core.Domain.Medical
{
    public partial class AreasName : BaseEntity
    {
        public int CityId { get; set; }
        public string Name { get; set; }
    }
}
