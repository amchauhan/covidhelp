﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Nop.Core.Domain.Medical
{
    public partial class Equipments : BaseEntity
    {
        public int CustomerId { get; set; }

        public int CategoryId { get; set; }

        public int Quantity { get; set; }
    }
}
